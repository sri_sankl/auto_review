# coding: utf-8
Encoding.default_external = 'utf-8'

require 'date'
require 'pp'
require 'csv'
require 'mysql2'
require 'net/http'
require 'json'
Japanese_regex = /\p{Hiragana}|\p{Katakana}|[?-]|[?-??]/

# ????
  # ???????????????????
  fields_file = "#{Dir.pwd}/lib/fields.tsv"
  Fields = CSV.read(fields_file, col_sep: "\t").delete_if{|r| r[0][0] == "/" }
  Fields_e = Fields.transpose[1]
  Fields_j = Fields.transpose[0]
  # ????????
  logic_file = "#{Dir.pwd}/lib/logic_brush.txt"
  #logic_file = "./logic_1.txt"

  Logics = JSON.parse(open(logic_file).read)

# ????????????????
class Divs
  S = Regexp.new(/\[.{1}\]/)
  SJ = Regexp.new(/\([?????]\)/)
  N = Regexp.new(/\(\d{1}\)/)
  AU = Regexp.new(/\([A-Z]\)/)
  AL = Regexp.new(/\([a-z]\)/)
#  OTHER_SYM = Regexp.new(/\.{1}\)/)
end

class Hash
  # ??????????????????
  def self.rnew
    self.new { |h,k| h[k] = Hash.new(&h.default_proc) }
  end
end

# ??????????????????????String class???
class String
  @@divs = Divs.constants

  def cleanup
    return self.gsub(" ", " ").strip
  end

  def structurize
    dn, de, i = nil, nil, self.length
    @@divs.each do |div_name|
      div_exp = Divs.const_get(div_name)
      index = self.index(div_exp)
      dn, de, i = div_name, div_exp, index if !index.nil? && index < i
    end

    div_arr, content_arr = self.scan(de), self.split(de)
    content_arr.size.downto(1) do |i|
      content_arr[i] = content_arr[i+1] if content_arr[i] == ""
    end

    contents1, contents2 = [], []
    contents1 << {t: content_arr[0].to_s} if content_arr[0] != ""
    (0..div_arr.size-1).each do |i|
      d, c = div_arr[i], content_arr[i+1].to_s
      contents1 << {dn.to_sym => d, t: c}
    end

    contents1.each do |c1|
      c1[:t].to_s.structurize.each do |c2|
        contents2 << c1.merge(c2) {|k, old, new| new}
      end
    end
    return contents2
  rescue
    #p $!
    return [{t: self.cleanup}] if self.cleanup != ""
  end
end

# ?????????
class Genko_raw < Array
  @@fields = Hash[Fields_e.collect.zip(Fields_j)]

  def sub(genko)
    return @genko = genko
  end

  def structurize
    structured = Genko_structured.new
    @@fields.each do |k, v|
      begin
        @genko[k].structurize.each do |s|
          structured << {F: v}.merge(s) if !s.nil? 
        end
      rescue 
        #p $!
        next
      end
    end
    return structured
  end
end 

# ????????????????????
class Genko_structured < Genko_raw
  def evaluate_cassette(cassette)
    logic = cassette["??"].values.join(" && ") rescue cassette["??"]
    output = []
    compare = cassette["??"][0] == "-" 
    if cassette["??"][2..-2] == "????"
      fields = Fields_j.reject {|f| f =~ /PE??No|NG_NO|NG_COMMENT/ }
    else
      fields = cassette["??"][2..-2].split("|")
    end
    (0..self.size-1).each do |i|
      if !fields.include?(self[i][:F]) 
        output[i] = nil
      else
        txt = self[i][:t]
        skshu = (self[i][:S].nil? ? "" : self[i][:S])
        number = (self[i][:N].nil? ? "" : self[i][:N]) 
        alpha = (self[i][:AU].nil? ? "" : self[i][:AU])
        begin
          output[i] = eval(logic)
          output[i] = nil if output[i] == false 
        rescue Exception 
          output[i] = nil
        end
      end
    end
    return output
  end

  def evaluate_cassettes(cassettes)
    output = {}
    cassettes.each do |k, v|
      output[v["?"]] = self.evaluate_cassette(v)
    end
    return output
  end

  def evaluate_cassettes_show(cassettes)
    output = Hash.rnew 
    self.evaluate_cassettes(cassettes).each do |k, v|
      v.each do |a|
        output[k]["????: #{a.to_s}"] = self[v.index(a)] if !a.nil?
      end
    end 
    return output
  end

  def evaluate_cassettes_hit(cassettes)
    output = false
    self.evaluate_cassettes(cassettes).each do |k, v|
      v.each do |a|
        if !a.nil?
          output = true
          break
        end
      end
    end 
    return output
  end

  def evaluate_rule(rule)
    if !rule["??"].nil?
      zentei = self.evaluate_cassettes_hit(rule["??"])
      return {} if !zentei
    end
    return self.evaluate_cassettes_show(rule["????"])
  end

  def evaluate_rules_all(rules)
    output = []
    rules.each do |k, v|
      self.evaluate_rule(v).each do |k2, v2|
        next if k2.nil?
        output << {
          "H_SNK_NO" => self.select{|h| h[:F] == "PE??No"}[0][:t],
          "NG_NO" => k,
          "NG_COMMENT" => k2 + ":" + v2.to_s}
      end
    end
    return output
  end

  def evaluate_rules_summary(rules, output)
    rules.each do |k, v|
      if self.evaluate_rule(v) != {}
        output[k] += 1 
      end
    end
    return output
  end
end

my = Mysql2::Client.new(:host => "localhost",
                        :username =>"root",
                        :database =>"rec_2")

debug_mode = true

twht = ARGV[0]
limit = ARGV[1].to_i
target_rule_gen = ARGV[2]
target_rule_alg = ARGV[3]

if target_rule_gen == "all"
  tbl = "genko_#{twht}_7"
else
  begin
    my.query(%!DROP TABLE genko_#{twht}_1_G!)
  rescue
  end
  my.query(%!CREATE TABLE genko_#{twht}_1_G SELECT * FROM genko_#{twht}_1 WHERE NG_NO like "%#{target_rule_gen}%"!)
  my.query(%!ALTER TABLE genko_#{twht}_1_G DROP COLUMN id!)
  my.query(%!ALTER TABLE genko_#{twht}_1_G ADD COLUMN id INT(11) AUTO_INCREMENT KEY!)
  tbl = "genko_#{twht}_1_G"
end

logics = {}
if target_rule_alg == "all"
  logics = Logics
else
  Logics.each do |k, v|    
    logics[k] = v if k.include? target_rule_alg
  end
end

gr = Genko_raw.new
result = []

start_time = Time.now

my.query(%!TRUNCATE TABLE output_#{twht}!)

bulk_size = 100

ngs = 0
min, max = 0, 0
loop do 
  min, max = max + 1, max + bulk_size
  break if max > limit && debug_mode
  genkos = my.query("SELECT * FROM #{tbl} where id between #{min} and #{max}")
  break if genkos.count == 0

  genkos.each do |g|
    gr.sub(g)
    gr_str = gr.structurize
    result = gr_str.evaluate_rules_all(logics)
    result.each do |r|
      h, n, c = r["H_SNK_NO"], r["NG_NO"], my.escape(r["NG_COMMENT"])
      my.query(%!INSERT INTO output_#{twht} (H_SNK_NO, NG_NO, NG_COMMENT) VALUES ('#{h}','#{n}','#{c}')!)
    end
    ngs += result.size
  end

  p "#{min - 1 + genkos.count}: #{ngs}NGs, #{Time.now - start_time}s"
end

begin
  my.query(%!DROP TABLE result_#{twht}!)
rescue
end

if twht == "TW"
  my.query(%|
    create table result_#{twht}
    select
      T1.NG_NO as GEN_NG_NO,
      T1.NG_COMMENT as GEN_NG_COMMENT,
      T2.NG_NO as ALG_NG_NO,
      T2.NG_COMMENT as ALG_NG_COMMENT,
      T1.H_SNK_NO,
      T1.KEISAI_CO_ME,
      T1.KOTSU,
      T1.SKSHU,
      T1.KYUYO,
      T1.CATCH,
      T1.HONMON_KOTSU,
      T1.HONMON_SKSHU,
      T1.HONMON_KYUYO,
      T1.KINMUCHI,
      T1.KINMU_JIKAN,
      T1.KYUKA,
      T1.SHIKAKU,
      T1.TAIGU,
      T1.OBO,
      T1.SONOTA,
      T1.RENRAKU_SAKI,
      T1.SYOZCHI
    from 
      (select * from #{tbl} limit #{limit}) as T1 
      left join 
      output_#{twht} as T2 
      on T1.H_SNK_NO = T2.H_SNK_NO
    order by
      GEN_NG_NO, GEN_NG_COMMENT, ALG_NG_NO, ALG_NG_COMMENT
    |)
else
  my.query(%|
    create table result_#{twht}
    select
      T1.NG_NO as GEN_NG_NO,
      T1.NG_COMMENT as GEN_NG_COMMENT,
      T2.NG_NO as ALG_NG_NO,
      T2.NG_COMMENT as ALG_NG_COMMENT,
      T1.H_SNK_NO,
      T1.KEISAI_CO_ME,
      T1.SHIGOTO_NAIYO,
      T1.TSTNR_KATA,
      T1.AKUSESU,
      T1.SKSHU,
      T1.KYUYO,
      T1.KINMUCHI,
      T1.KINMU_JIKAN,
      T1.KYUKA,
      T1.TAIGU,
      T1.RENRAKU_SAKI,
      T1.SYOZCHI
    from 
      (select * from #{tbl} limit #{limit}) as T1
      left join 
      output_#{twht} as T2 
      on T1.H_SNK_NO = T2.H_SNK_NO
    order by
      GEN_NG_NO, GEN_NG_COMMENT, ALG_NG_NO, ALG_NG_COMMENT
    |)
end

