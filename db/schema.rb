# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160204193924) do

  create_table "genko_hts", force: :cascade do |t|
    t.string  "H_SNK_NO",             limit: 8
    t.string  "TW_ARIA_KR_CD",        limit: 4
    t.string  "TW_ARIA_KR_MS",        limit: 50
    t.string  "SHN_CD",               limit: 5
    t.string  "SHN_RYK_MS",           limit: 20
    t.string  "GO_CD",                limit: 6
    t.string  "GO_RYK_MS",            limit: 20
    t.date    "KEISAI_KAISHI_HI"
    t.date    "KEISAI_SHURYO_HI"
    t.string  "SSKKK_CD",             limit: 4
    t.string  "SSKKK_RYK_MS",         limit: 10
    t.string  "MS_CD",                limit: 4
    t.string  "MS_RYK_MS",            limit: 10
    t.string  "EGYO_KT_CD",           limit: 6
    t.string  "EGYO_KT_MS",           limit: 40
    t.string  "EGYO_SS_CD",           limit: 8
    t.string  "EGYO_SS_MS",           limit: 40
    t.string  "EGYO_TT_CD",           limit: 8
    t.string  "EGYO_TT_MS",           limit: 81
    t.string  "SSK_KT_CD",            limit: 6
    t.string  "SSK_KT_MS",            limit: 40
    t.string  "SSK_SS_CD",            limit: 8
    t.string  "SSK_SS_MS",            limit: 40
    t.string  "SSK_TT_CD",            limit: 8
    t.string  "SSK_TT_MS",            limit: 81
    t.string  "KEISAI_CO_ME",         limit: 100
    t.string  "MEMO_TX",              limit: 60
    t.string  "JC_SNK_NO",            limit: 10
    t.string  "KEISAISA_KYK_CD",      limit: 10
    t.string  "JCSA_KYK_CD",          limit: 10
    t.string  "RYUYOMO_H_SNK_NO",     limit: 8
    t.string  "H_OBO_SAKI_NO",        limit: 8
    t.string  "OBO_SAKI_KYK_CD",      limit: 10
    t.string  "OBO_SB_CD",            limit: 2
    t.string  "NET_OBO_UKETSUKE_FG",  limit: 1
    t.string  "MAP_HYJ_UMU_FG",       limit: 1
    t.string  "SKSHU",                limit: 200
    t.text    "SHIGOTO_NAIYO",        limit: 65535
    t.text    "TSTNR_KATA",           limit: 65535
    t.text    "KINMUCHI",             limit: 65535
    t.text    "AKUSESU",              limit: 65535
    t.text    "KINMU_JIKAN",          limit: 65535
    t.text    "KYUYO",                limit: 65535
    t.text    "KYUKA",                limit: 65535
    t.text    "TAIGU",                limit: 65535
    t.text    "SAIYO_YT_NINZU_TX",    limit: 65535
    t.text    "SYOZCHI",              limit: 65535
    t.text    "OBO",                  limit: 65535
    t.text    "RENRAKU_SAKI",         limit: 65535
    t.string  "KYKT_CD_1",            limit: 2
    t.string  "KYKT_CD_2",            limit: 2
    t.string  "KYKT_CD_3",            limit: 2
    t.string  "SKSHU_CD_1",           limit: 5
    t.string  "SKSHU_CD_2",           limit: 5
    t.string  "SKSHU_CD_3",           limit: 5
    t.string  "KYUYO_SB_CD_1",        limit: 2
    t.string  "KYUYO_SB_CD_2",        limit: 2
    t.string  "KYUYO_SB_CD_3",        limit: 2
    t.integer "KYUYO_KG_1",           limit: 4
    t.integer "KYUYO_KG_2",           limit: 4
    t.integer "KYUYO_KG_3",           limit: 4
    t.string  "KINMUCHI_CD_1",        limit: 5
    t.string  "KINMUCHI_CD_2",        limit: 5
    t.string  "KINMUCHI_CD_3",        limit: 5
    t.string  "KINMUCHI_1_EKI_CD_1",  limit: 7
    t.string  "KINMUCHI_1_EKI_CD_2",  limit: 7
    t.string  "KINMUCHI_1_EKI_CD_3",  limit: 7
    t.string  "KINMUCHI_2_EKI_CD_1",  limit: 7
    t.string  "KINMUCHI_2_EKI_CD_2",  limit: 7
    t.string  "KINMUCHI_2_EKI_CD_3",  limit: 7
    t.string  "KINMUCHI_3_EKI_CD_1",  limit: 7
    t.string  "KINMUCHI_3_EKI_CD_2",  limit: 7
    t.string  "KINMUCHI_3_EKI_CD_3",  limit: 7
    t.string  "PREF_DEL_FG",          limit: 1
    t.date    "PREF_SAKSE_NJ"
    t.string  "PREF_SAKSE_TT_CD",     limit: 50
    t.date    "PREF_UPD_NJ"
    t.string  "PREF_UPD_TT_CD",       limit: 50
    t.string  "PREF_UPD_MODULE_ID",   limit: 50
    t.string  "KINMUCHI_CD_4",        limit: 7
    t.string  "KINMUCHI_CD_5",        limit: 7
    t.string  "KINMUCHI_CD_6",        limit: 7
    t.string  "KINMUCHI_CD_7",        limit: 7
    t.string  "KINMUCHI_CD_8",        limit: 7
    t.string  "KINMUCHI_CD_9",        limit: 7
    t.string  "KINMUCHI_CD_10",       limit: 7
    t.string  "KINMUCHI_CD_11",       limit: 7
    t.string  "KINMUCHI_CD_12",       limit: 7
    t.string  "KINMUCHI_CD_13",       limit: 7
    t.string  "KINMUCHI_CD_14",       limit: 7
    t.string  "KINMUCHI_CD_15",       limit: 7
    t.string  "KINMUCHI_CD_16",       limit: 7
    t.string  "KINMUCHI_CD_17",       limit: 7
    t.string  "KINMUCHI_CD_18",       limit: 7
    t.string  "KINMUCHI_CD_19",       limit: 7
    t.string  "KINMUCHI_CD_20",       limit: 7
    t.string  "KINMUCHI_4_EKI_CD_1",  limit: 7
    t.string  "KINMUCHI_4_EKI_CD_2",  limit: 7
    t.string  "KINMUCHI_4_EKI_CD_3",  limit: 7
    t.string  "KINMUCHI_5_EKI_CD_1",  limit: 7
    t.string  "KINMUCHI_5_EKI_CD_2",  limit: 7
    t.string  "KINMUCHI_5_EKI_CD_3",  limit: 7
    t.string  "KINMUCHI_6_EKI_CD_1",  limit: 7
    t.string  "KINMUCHI_6_EKI_CD_2",  limit: 7
    t.string  "KINMUCHI_6_EKI_CD_3",  limit: 7
    t.string  "KINMUCHI_7_EKI_CD_1",  limit: 7
    t.string  "KINMUCHI_7_EKI_CD_2",  limit: 7
    t.string  "KINMUCHI_7_EKI_CD_3",  limit: 7
    t.string  "KINMUCHI_8_EKI_CD_1",  limit: 7
    t.string  "KINMUCHI_8_EKI_CD_2",  limit: 7
    t.string  "KINMUCHI_8_EKI_CD_3",  limit: 7
    t.string  "KINMUCHI_9_EKI_CD_1",  limit: 7
    t.string  "KINMUCHI_9_EKI_CD_2",  limit: 7
    t.string  "KINMUCHI_9_EKI_CD_3",  limit: 7
    t.string  "KINMUCHI_10_EKI_CD_1", limit: 7
    t.string  "KINMUCHI_10_EKI_CD_2", limit: 7
    t.string  "KINMUCHI_10_EKI_CD_3", limit: 7
    t.string  "KINMUCHI_11_EKI_CD_1", limit: 7
    t.string  "KINMUCHI_11_EKI_CD_2", limit: 7
    t.string  "KINMUCHI_11_EKI_CD_3", limit: 7
    t.string  "KINMUCHI_12_EKI_CD_1", limit: 7
    t.string  "KINMUCHI_12_EKI_CD_2", limit: 7
    t.string  "KINMUCHI_12_EKI_CD_3", limit: 7
    t.string  "KINMUCHI_13_EKI_CD_1", limit: 7
    t.string  "KINMUCHI_13_EKI_CD_2", limit: 7
    t.string  "KINMUCHI_13_EKI_CD_3", limit: 7
    t.string  "KINMUCHI_14_EKI_CD_1", limit: 7
    t.string  "KINMUCHI_14_EKI_CD_2", limit: 7
    t.string  "KINMUCHI_14_EKI_CD_3", limit: 7
    t.string  "KINMUCHI_15_EKI_CD_1", limit: 7
    t.string  "KINMUCHI_15_EKI_CD_2", limit: 7
    t.string  "KINMUCHI_15_EKI_CD_3", limit: 7
    t.string  "KINMUCHI_16_EKI_CD_1", limit: 7
    t.string  "KINMUCHI_16_EKI_CD_2", limit: 7
    t.string  "KINMUCHI_16_EKI_CD_3", limit: 7
    t.string  "KINMUCHI_17_EKI_CD_1", limit: 7
    t.string  "KINMUCHI_17_EKI_CD_2", limit: 7
    t.string  "KINMUCHI_17_EKI_CD_3", limit: 7
    t.string  "KINMUCHI_18_EKI_CD_1", limit: 7
    t.string  "KINMUCHI_18_EKI_CD_2", limit: 7
    t.string  "KINMUCHI_18_EKI_CD_3", limit: 7
    t.string  "KINMUCHI_19_EKI_CD_1", limit: 7
    t.string  "KINMUCHI_19_EKI_CD_2", limit: 7
    t.string  "KINMUCHI_19_EKI_CD_3", limit: 7
    t.string  "KINMUCHI_20_EKI_CD_1", limit: 7
    t.string  "KINMUCHI_20_EKI_CD_2", limit: 7
    t.string  "KINMUCHI_20_EKI_CD_3", limit: 7
    t.string  "NG_NO",                limit: 8
    t.text    "NG_COMMENT",           limit: 16777215
  end

  create_table "input_files", force: :cascade do |t|
    t.string   "file",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

end
