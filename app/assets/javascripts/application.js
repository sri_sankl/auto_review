// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.min
//= require bootstrap/js/bootstrap.min
//= require js.cookie.min
//= require angularjs/angular.min
//= require bootstrap-hover-dropdown/bootstrap-hover-dropdown.min
//= require jquery-slimscroll/jquery.slimscroll.min
//= require jquery.blockui.min
//= require uniform/jquery.uniform.min
//= require bootstrap-switch/js/bootstrap-switch.min
//= require app.min
//= require bootstrap-fileinput/bootstrap-fileinput
//= require bootstrap-select/js/bootstrap-select.min
//= require select2/js/select2.full.min
//= require bootbox/bootbox.min
//= require icheck/icheck.min
//= require scripts/components-select2.min
//= require scripts/form-icheck.min
//= require scripts/ui-bootbox.min
//= require layout3/scripts/layout.min
//= require layout3/scripts/demo.min
//= require global/scripts/quick-sidebar.min
//= require angular-file-upload.min
//= require angular-file-upload.min.map
//= require_tree .
