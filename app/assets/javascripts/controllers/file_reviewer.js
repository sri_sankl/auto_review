(function(angular, app) {
    "use strict";
    app.controller('fileReviewerController', ["$scope", "applicationService", "FileUploader",  function($scope, applicationService, FileUploader){
        var saveUrl = "/upload_file.json";
        var headers = {
                "X-CSRF-Token" : $('meta[name="csrf-token"]').attr('content')
        };

        $scope.uploadingFileName = "";
        $scope.uploader = new FileUploader({
            url: saveUrl,
            autoUpload: false,
            removeAfterUpload: true,
            alias: "file",
            headers: headers,
            onAfterAddingFile: function(item){
                $scope.uploadingFileName = item.file.name;
            }
        });

        $scope.uploader.onCompleteAll = function() {
            alert("File uploaded successfully.")
            console.log('onCompleteAll');
            $scope.loadData()
        };
        
        $scope.saveFile = function(){
            angular.forEach($scope.uploader.queue, function(item, key){                
                item.upload();
            });
        }
        
        var loadLatestFileKeys = function(){
            applicationService.getLatestFileData()
                .then(function(response){
                    $scope.latestKeys = response.data.keys;
                    console.log($scope.latestKeys)
                });
        }

        var loadExistingKeys = function(){
            applicationService.getExistingKeys()
                .then(function(response){
                    $scope.existingKeys = response.data.keys;
                    console.log($scope.existingKeys)
                });
        }

        $scope.loadData = function(){
            loadLatestFileKeys();
            loadExistingKeys();
        }

        $scope.loadData();

        $scope.reviewInputs = function(){
            applicationService.postReviewParams($scope.selectedLatestKeys, $scope.selectedExistingKeys, headers)
                .then(function(response){
                    alert("Submitted successfully, params: A: "+$scope.selectedLatestKeys + ", B: "+ $scope.selectedExistingKeys)
                });
        }

    }]);  
    
})(angular, autoReviewApp);
