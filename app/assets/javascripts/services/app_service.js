(function(angular, app) {
    "use strict";
    app.service("applicationService",["$http", function($http) {
        var getLatestFileData = function(){
            return $http.get("/get_latest_file_data.json")
        }
        
        var getExistingKeys = function(){
            return $http.get("/get_exiting_ng_nos.json")
        }

        var postReviewParams = function(latest_keys, existing_keys, headers){
            return $http.post("/post_review_params.json", {selected_latest_keys: latest_keys, selected_existing_keys: existing_keys }, {headers: headers})
        }


        return {
            getLatestFileData : getLatestFileData,
            getExistingKeys : getExistingKeys,
            postReviewParams : postReviewParams
        };
    }]);
})(angular, autoReviewApp);
