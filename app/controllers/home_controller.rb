class HomeController < ApplicationController

  def index
    @input_file = InputFile.new
  end

  def index2
    @input_file = InputFile.new
  end
  
  def upload_file
    @input_file = InputFile.new(params.permit(:file))
    respond_to do |format|
      format.json do 
        status = @input_file.save
        render :json => {:status => status, :id => @input_file.try(:id)}
      end
    end
  end

  def get_latest_file_data
    respond_to do |format|
      format.json do 
        @input_file = InputFile.order("id DESC").first
        data = []
        if @input_file.present?
          contents =  @input_file.file.read
          contents = contents.force_encoding("ISO-8859-1").encode("UTF-8")
          contents = JSON.parse(contents)
          pattern = /^(?=.*\D)[-\w]+$/
          refined_keys = contents.keys.select{|key| pattern =~ key}
          data = key_grouper(refined_keys)
        else
          data = []
        end
        render :json => {:keys => data}
      end
    end
  end

  def get_exiting_ng_nos
    respond_to do |format|
      format.json do 
        existing_data = GenkoHT.select("DISTINCT ng_no").map(&:ng_no)
        render :json => {:keys => key_grouper(existing_data)}
      end
    end
  end

  def update_review_params
    p "===========params=========="
    p params
    p "==========================="
    p File.dirname(__FILE__)
    file = File.expand_path('../../lib/pr_brush.rb', File.dirname(__FILE__))
    eval File.read(file)
    respond_to do |format|
      format.json do 
        render :json => {:status => true}
      end
    end

  end

  private

  def key_grouper(array)
   data = []
    array.group_by{|k| k[0].to_s}.sort.each do |key, values|
      if key.present?
        values.each do |v| 
          inner_data = {}
          inner_data[:value] = v 
          inner_data[:group] = key
          data.push(inner_data)
        end
      end
    end
    data
  end
end
